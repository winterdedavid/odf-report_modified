module ODFReport

module Parser


  # Default HTML parser
  #
  # sample HTML
  #
  # <p> first paragraph </p>
  # <p> second <strong>paragraph</strong> </p>
  # <blockquote>
  #     <p> first <em>quote paragraph</em> </p>
  #     <p> first quote paragraph </p>
  #     <p> first quote paragraph </p>
  # </blockquote>
  # <p> third <strong>paragraph</strong> </p>
  #
  # <p style="margin: 100px"> fourth <em>paragraph</em> </p>
  # <p style="margin: 120px"> fifth paragraph </p>
  # <p> sixth <strong>paragraph</strong> </p>
  #

  class Default

    attr_accessor :paragraphs

    def initialize(text, template_node)
      @text = text
      @paragraphs = []
      @template_node = template_node

      parse
    end

    def parse
      xml = @template_node.parse(@text)

      xml.css("p", "h1", "h2").each do |p|

        style = check_style(p)
        text = parse_formatting(p.inner_html)

        add_paragraph(text, style)
      end
    end

    def add_paragraph(text, style)

      node = @template_node.dup

      node['text:style-name'] = style if style
      node.children = text

      @paragraphs << node
    end

    private

    def parse_formatting(text)
      text.strip!
      # text.gsub!(/<b>(.+?)<\/b>/)  { "<style:style style:name=\"bold\">#{$1}<\/style:style>" }

      regex = /<\/?[biu]>/
      previous_tag = text.index(regex)
      current_tag = ""
      if(previous_tag != nil)
        # Style consists of multiple HTML tag letter based on nesting
        style = text[previous_tag + 1]
        text.sub!(regex) { "<text:span text:style-name=\"" + style + "\">" }

        while(previous_tag != nil && text.index(regex) != nil)
          current_tag = text.index(regex)
          char = text[current_tag + 1]
            # current tag is closing tag
            if char == "\/"
              current_tag_letter = text[current_tag + 2]
              previous_tag_letter = style[style.length - 1]

              # remove tag from style
              style.sub!(current_tag_letter, "")

              if style.empty?
                text.sub!(regex) { "<\/text:span>" }
              else
                text.sub!(regex) { "<\/text:span><text:span text:style-name=\"" + style.chars.sort.join + "\">" }
              end
            # current tag is non-closing tag
            else
              current_tag_letter = char
              style = style + current_tag_letter;
              # first level tag
              if style.length == 1
                text.sub!(regex) { "<text:span text:style-name=\"" + current_tag_letter + "\">" }
              # nested tag
              else
                text.sub!(regex) { "<\/text:span><text:span text:style-name=\"" + style.chars.sort.join + "\">" }
              end
            end
            previous_tag = current_tag
            current_tag = current_tag + 1
        end
      end

      text
    end

    def check_style(node)
      style = nil

      if node.name =~ /h\d/i
        style = "title"

      elsif node.parent && node.parent.name == "blockquote"
        style = "quote"

      elsif node['style'] =~ /margin/
        style = "quote"

      end

      style
    end

  end

end

end